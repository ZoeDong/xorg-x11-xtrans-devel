%global debug_package %{nil}

Summary:       X.Org X11 developmental X transport library
Name:          xorg-x11-xtrans-devel
Version:       1.5.0
Release:       1%{?dist}
License:       MIT
URL:           http://www.x.org
Source0:       https://xorg.freedesktop.org/archive/individual/lib/xtrans-%{version}.tar.xz
Patch3000:     xtrans-1.0.3-avoid-gethostname.patch

BuildRequires: make pkgconfig xorg-x11-util-macros
BuildArch:     noarch

%description
X.Org X11 developmental X transport library


%prep
%autosetup -n xtrans-%{version} -p1


%build
%configure --libdir=%{_datadir} --disable-docs


%install
%{make_install}


%files
%license COPYING
%doc AUTHORS ChangeLog README.md
%dir %{_includedir}/X11
%dir %{_includedir}/X11/Xtrans
%{_includedir}/X11/Xtrans/Xtrans.c
%{_includedir}/X11/Xtrans/Xtrans.h
%{_includedir}/X11/Xtrans/Xtransint.h
%{_includedir}/X11/Xtrans/Xtranslcl.c
%{_includedir}/X11/Xtrans/Xtranssock.c
%{_includedir}/X11/Xtrans/Xtransutil.c
%{_includedir}/X11/Xtrans/transport.c
%{_datadir}/aclocal/xtrans.m4
%{_datadir}/pkgconfig/xtrans.pc


%changelog
* Mon Aug 21 2023 Miaojun Dong <zoedong@tencent.com> - 1.5.0-1
- Bump version to 1.5.0

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.4.0-2
- Rebuilt for OpenCloudOS Stream 23

* Mon Feb 20 2023 rockerzhu <rockerzhu@tencent.com> - 1.4.0-1
- Initial build
